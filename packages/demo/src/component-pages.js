const componentPages = [
  {
    title: "基础组件",
    children: [
      {
        title: "Button 按钮",
        name: "Button",
        path: "/pages/button/index",
      },
      {
        title: "Cell 单元格",
        name: "Cell",
        path: "/pages/cell/index",
      },
      {
        title: "Icon 图标",
        name: "Icon",
        path: "/pages/icon/index",
      },
      {
        title: "Image 图片",
        name: "Image",
        path: "/pages/image/index",
      },
      {
        title: "Layout 布局",
        name: "Layout",
        path: "/pages/layout/index",
      },
      {
        title: "Popup 弹出层",
        name: "Popup",
        path: "/pages/popup/index",
      },
      {
        title: "Style 内置样式",
        name: "Style",
        path: "/pages/style/index",
      },
      {
        title: "Toast 轻提示",
        name: "Toast",
        path: "/pages/toast/index",
      },
      {
        title: "Space 间距",
        name: "Space",
        path: "/pages/space/index",
      },
    ],
  },
  {
    title: "反馈组件",
    children: [
      {
        title: "ActionSheet 动作面板",
        name: "ActionSheet",
        path: "/pages/action-sheet/index",
      },
      {
        title: "Dialog 弹出框",
        name: "Dialog",
        path: "/pages/dialog/index",
      },
      {
        title: "Loading 加载",
        name: "Loading",
        path: "/pages/loading/index",
      },
      {
        title: "Notify 消息提示",
        name: "Notify",
        path: "/pages/notify/index",
      },
      {
        title: "Backdrop 背景暗化",
        name: "Backdrop",
        path: "/pages/backdrop/index",
      },
      {
        title: "ShareSheet 分享面板",
        name: "ShareSheet",
        path: "/pages/share-sheet/index",
      },
      {
        title: "SwipeCell 滑动单元格",
        name: "SwipeCell",
        path: "/pages/swipe-cell/index",
      },
    ],
  },
  {
    title: "展示组件",
    children: [
      {
        title: "Badge 徽标",
        name: "Badge",
        path: "/pages/badge/index",
      },
      {
        title: "Collapse 折叠面板",
        name: "Collapse",
        path: "/pages/collapse/index",
      },
      {
        title: "CountDown 倒计时",
        name: "CountDown",
        path: "/pages/count-down/index",
      },
      {
        title: "Divider 分割线",
        name: "Divider",
        path: "/pages/divider/index",
      },
      {
        title: "Empty 空状态",
        name: "Empty",
        path: "/pages/empty/index",
      },
      {
        title: "NoticeBar 通知栏",
        name: "NoticeBar",
        path: "/pages/notice-bar/index",
      },
      {
        title: "Progress 进度条",
        name: "Progress",
        path: "/pages/progress/index",
      },
      {
        title: "Skeleton 骨架屏",
        name: "Skeleton",
        path: "/pages/skeleton/index",
      },
      {
        title: "Steps 步骤条",
        name: "Steps",
        path: "/pages/steps/index",
      },
      {
        title: "Sticky 粘性布局",
        name: "Sticky",
        path: "/pages/sticky/index",
      },
      {
        title: "Swiper 轮播",
        name: "Swiper",
        path: "/pages/swiper/index",
      },
      {
        title: "Tag 标签",
        name: "Tag",
        path: "/pages/tag/index",
      },
    ],
  },
  {
    title: "导航组件",
    children: [
      {
        title: "Grid 宫格",
        name: "Grid",
        path: "/pages/grid/index",
      },
      {
        title: "Navbar 导航栏",
        name: "Navbar",
        path: "/pages/navbar/index",
      },
      {
        title: "Pagination 分页",
        name: "Pagination",
        path: "/pages/pagination/index",
      },
      {
        title: "Sidebar 侧边导航",
        name: "Sidebar",
        path: "/pages/sidebar/index",
      },
      {
        title: "Tabs 标签页",
        name: "Tabs",
        path: "/pages/tabs/index",
      },
      {
        title: "Tabbar 标签栏",
        name: "Tabbar",
        path: "/pages/tabbar/index",
      },
      {
        title: "TreeSelect 分类选择",
        name: "TreeSelect",
        path: "/pages/tree-select/index",
      },
    ],
  },
]

module.exports = componentPages
// eslint-disable-next-line import/no-commonjs
module.exports.default = componentPages
